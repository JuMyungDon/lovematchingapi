package com.md.lovematchingapi.service;

import com.md.lovematchingapi.entity.Member;
import com.md.lovematchingapi.model.member.MemberCreateRequest;
import com.md.lovematchingapi.repository.MemberRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class MemberService {
    private final MemberRepository memberRepository;

    public Member getData(long id){
        return memberRepository.findById(id).orElseThrow();
    }

    public void setMember(MemberCreateRequest request){
        Member addData = new Member();
        addData.setName(request.getName());
        addData.setPhoneNumber(request.getPhoneNumber());
        addData.setGender(request.getGender());
        addData.setJob(request.getJob());
        addData.setAppearance(request.getAppearance());
        addData.setHeight(request.getHeight());
        addData.setAge(request.getAge());
        addData.setMbti(request.getMbti());

        memberRepository.save(addData);
    }
}
