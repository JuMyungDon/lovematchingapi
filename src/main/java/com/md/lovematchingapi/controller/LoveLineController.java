package com.md.lovematchingapi.controller;

import com.md.lovematchingapi.entity.Member;
import com.md.lovematchingapi.model.loveLine.LoveLineCreateRequest;
import com.md.lovematchingapi.model.loveLine.LoveLineItem;
import com.md.lovematchingapi.model.loveLine.LoveLinePhoneNumberChangeRequest;
import com.md.lovematchingapi.model.loveLine.LoveLineResponse;
import com.md.lovematchingapi.service.LoveLineService;
import com.md.lovematchingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/love-line")
public class LoveLineController {
    private final MemberService memberService;
    private final LoveLineService loveLineService;

    @PostMapping("/new/member-id/{memberId}")
    public String setLoveLine(@PathVariable long memberId, @RequestBody LoveLineCreateRequest request){
        Member member = memberService.getData(memberId);
        loveLineService.setLoveLine(member, request);

        return "ok";
    }

    @GetMapping("/all")
    public List<LoveLineItem> getLoveLines(){
        return loveLineService.getLoveLines();
    }

    @GetMapping("/be-selected/{id}")
    public LoveLineResponse getLoveLine(@PathVariable long id){
        return loveLineService.getLoveLine(id);
    }

    @PutMapping("/PhoneNumber/love-lind-id/{loveLineId}")
    public String putPhoneNumber(@PathVariable long loveLineId, @RequestBody LoveLinePhoneNumberChangeRequest request){
        loveLineService.putPhoneNumber(loveLineId, request);

        return "ok";
    }
}
