package com.md.lovematchingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Appearance {
    THE_BEST("최상")
    ,BEST("상")
    ,MIDDLE("중")
    ,LOWEST("중하")
    ,THE_LOWEST("하");

    private final String name;
}
