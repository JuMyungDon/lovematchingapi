package com.md.lovematchingapi.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum Mbti {
    ISTJ("ISTJ")
    ,ISTP("ISTP")
    ,INFJ("INFJ")
    ,INTJ("INTJ")
    ,ISFJ("ISFJ")
    ,ISFP("ISFP")
    ,INFP("INFP")
    ,INTP("INTP")
    ,ESTJ("ESTJ")
    ,ESFP("ESFP")
    ,ENFP("ENFP")
    ,ENTP("ENTP")
    ,ESFJ("ESFJ")
    ,ESTP("ESTP")
    ,ENFJ("ENFJ")
    ,ENTJ("ENTJ");

    private final String Name;
}
