package com.md.lovematchingapi.model.loveLine;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLinePhoneNumberChangeRequest {
    private String lovePhoneNumber;
}
