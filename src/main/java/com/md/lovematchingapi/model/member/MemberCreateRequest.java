package com.md.lovematchingapi.model.member;

import com.md.lovematchingapi.enums.Appearance;
import com.md.lovematchingapi.enums.Gender;
import com.md.lovematchingapi.enums.Mbti;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MemberCreateRequest {
    private String name;
    private String phoneNumber;
    private Gender gender;
    private String job;
    private Appearance appearance;
    private Float height;
    private Byte age;
    private Mbti mbti;
}
