package com.md.lovematchingapi.entity;

import com.md.lovematchingapi.enums.Appearance;
import com.md.lovematchingapi.enums.Gender;
import com.md.lovematchingapi.enums.Mbti;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

import javax.naming.Name;

@Entity
@Getter
@Setter
public class Member {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 20)
    private String name;

    @Column(nullable = false, length = 13, unique = true)
    private String phoneNumber;

    @Column(nullable = false)
    private Gender gender;

    @Column(nullable = false, length = 15)
    private String job;

    @Column(nullable = false)
    private Appearance appearance;

    @Column(nullable = false)
    private Float height;

    @Column(nullable = false)
    private Byte age;

    @Column(nullable = false)
    private Mbti mbti;
}
