package com.md.lovematchingapi.entity;

import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;

@Entity
@Getter
@Setter
public class LoveLine {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "memberId", nullable = false) // BD에 컬럼 이름이 이렇게 생성 됨(Jpa가 자동매칭 시켜줌)
    private Member member; // 불러 올 때는 Member로 불러 옮

    @Column(nullable = false, length = 13)
    private String lovePhoneNumber;
}
